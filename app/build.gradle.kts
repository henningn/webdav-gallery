plugins {
  id("com.android.application")
  id("org.jetbrains.kotlin.android")
}

android {
  namespace = "com.nextcloud.gallery"
  compileSdk = 33

  defaultConfig {
    applicationId = "com.nextcloud.gallery"
    minSdk = 33
    targetSdk = 33
    versionCode = 1
    versionName = "0.1.0"

    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    vectorDrawables {
      useSupportLibrary = true
    }
  }

  buildTypes {
    named("release") {
      isMinifyEnabled = false
      setProguardFiles(listOf(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"))
    }
  }

  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
  }

  kotlinOptions {
    jvmTarget = "17"
  }

  buildFeatures {
    compose = true
  }

  composeOptions {
    kotlinCompilerExtensionVersion = "1.4.3"
  }

  packagingOptions {
    resources {
      excludes += "/META-INF/{AL2.0,LGPL2.1}"
    }
  }
}

dependencies {
  val composeUiVersion by extra("1.4.3")

  implementation("androidx.core:core-ktx:1.10.1")
  implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
  implementation("androidx.activity:activity-compose:1.7.2")
  implementation("androidx.compose.ui:ui:${composeUiVersion}")
  implementation("androidx.compose.ui:ui-tooling-preview:${composeUiVersion}")
  implementation("androidx.compose.material3:material3:1.1.1")
  implementation("androidx.navigation:navigation-compose:2.6.0")
  implementation("com.github.nextcloud:android-library:2.14.0") {
    exclude(group = "org.ogce", module = "xpp3")
  }
  compileOnly("commons-httpclient:commons-httpclient:3.1")
  testImplementation("junit:junit:4.13.2")
  androidTestImplementation("androidx.test.ext:junit:1.1.5")
  androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
  androidTestImplementation("androidx.compose.ui:ui-test-junit4:${composeUiVersion}")
  debugImplementation("androidx.compose.ui:ui-tooling:${composeUiVersion}")
  debugImplementation("androidx.compose.ui:ui-test-manifest:${composeUiVersion}")
}
