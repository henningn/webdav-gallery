package com.nextcloud.gallery.login

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UrlParserTest {

  @Test
  fun parseExpectedUrl() {
    val urlParser = UrlParser("nc://login/server:https://example.com&user:username&password:pwstring")
    Assert.assertEquals("example.com", urlParser.getServerAddress())
    Assert.assertEquals("username", urlParser.getUsername())
    Assert.assertEquals("pwstring", urlParser.getPassword())
  }

  @Test
  fun parseUrlWww() {
    val urlParser = UrlParser("nc://login/server:https://www.example.com&user:username&password:pwstring")
    Assert.assertEquals("www.example.com", urlParser.getServerAddress())
    Assert.assertEquals("username", urlParser.getUsername())
    Assert.assertEquals("pwstring", urlParser.getPassword())
  }

  @Test
  fun parseUrlWithSlash() {
    val urlParser = UrlParser("nc://login/server:https://example.com/&user:username&password:pwstring")
    Assert.assertEquals("example.com", urlParser.getServerAddress())
    Assert.assertEquals("username", urlParser.getUsername())
    Assert.assertEquals("pwstring", urlParser.getPassword())
  }

  @Test
  fun parseUserPasswordSwapped() {
    val urlParser = UrlParser("nc://login/server:https://example.com&password:pwstring&user:username")
    Assert.assertEquals("example.com", urlParser.getServerAddress())
    Assert.assertEquals("username", urlParser.getUsername())
    Assert.assertEquals("pwstring", urlParser.getPassword())
  }
}
