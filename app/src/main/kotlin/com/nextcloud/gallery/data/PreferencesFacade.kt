package com.nextcloud.gallery.data

import android.content.SharedPreferences

private const val SERVER_ADDRESS = "serverAddress"
private const val USER = "user"
private const val PASSWORD = "password"

class PreferencesFacade(private val preferences: SharedPreferences) {

  fun saveLoginData(serverAddress: String, user: String, password: String) {
    preferences.edit()
      .putString(SERVER_ADDRESS, serverAddress)
      .putString(USER, user)
      .putString(PASSWORD, password)
      .apply()
  }

  fun getServerAddress(): String {
    return getString(SERVER_ADDRESS)
  }

  fun getUser(): String {
    return getString(USER)
  }

  fun getPassword(): String {
    return getString(PASSWORD)
  }

  private fun getString(key: String): String {
    val string = preferences.getString(key, null)
    return if (string.isNullOrEmpty()) "" else string
  }
}
