package com.nextcloud.gallery.data

import com.nextcloud.gallery.ui.model.Directory

class DummyData() {
  val imagesList = ArrayList<String>()
  val directories = ArrayList<Directory>()

  init {
    for (i in 0..1000) {
      imagesList.add(i.toString())
    }

    directories.add(Directory("/Photobook/2006 Dagordacil Gildentreffen", false))
    directories.add(Directory("/Photobook/2007 Gildentreffen", false))
    directories.add(Directory("/Photobook/2013 Canada", false))
    directories.add(Directory("/Photobook/2013-08-25 Honda CBF 600", false))
    directories.add(Directory("/Photobook/2014-07-06 Papenburg", false))
    directories.add(Directory("/Photobook/2015-08-09 Papenburg", false))
    directories.add(Directory("/Photobook/2016-07-09 Oschersleben", false))
    directories.add(Directory("/Photobook/2016-08-07 Papenburg", false))
    directories.add(Directory("/Photobook/2017-07-02 Papenburg", false))
    directories.add(Directory("/Photobook/2017-08-14 Oschersleben", true))
    directories.add(Directory("/Photobook/2019-07-11 Oschersleben", false))
    directories.add(Directory("/Photobook/2022-08-05 Suzuki GSX-R 750", false))
    directories.add(Directory("/Photobook/Leo", false))
    directories.add(Directory("/Photobook/Udo", false))
    directories.add(Directory("/Photobook/Wolfgang", false))
  }
}
