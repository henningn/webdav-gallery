package com.nextcloud.gallery

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.nextcloud.common.NextcloudClient
import com.nextcloud.common.OkHttpCredentialsUtil
import com.nextcloud.gallery.data.PreferencesFacade
import com.nextcloud.gallery.ui.model.NavDestinations
import com.nextcloud.gallery.ui.screen.LoginScreen
import com.nextcloud.gallery.ui.screen.LoginWebViewScreen
import com.nextcloud.gallery.ui.screen.MainScreen
import com.nextcloud.gallery.ui.state.GlobalViewModel
import com.nextcloud.gallery.ui.theme.NextcloudGalleryTheme
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.resources.files.ReadFolderRemoteOperation

private const val TAG = "MainActivity"

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val preferences = PreferencesFacade(getPreferences(Context.MODE_PRIVATE))
    val globalViewModel = GlobalViewModel()

    setContent {
      val navController: NavHostController = rememberNavController()

      Log.d(TAG, "saved user: " + preferences.getUser())

      val startDestination =
        if (preferences.getUser().isEmpty()) {
          NavDestinations.loginUrl.name
        } else {
          NavDestinations.main.name
        }

      NextcloudGalleryTheme {
        NavHost(
          navController = navController,
          startDestination = startDestination,
        ) {
          composable(route = NavDestinations.loginUrl.name) {
            LoginScreen(globalViewModel, navController)
          }
          composable(route = NavDestinations.loginWebView.name) {
            LoginWebViewScreen(
              preferences = preferences,
              navController = navController,
              globalViewModel = globalViewModel
            )
          }
          composable(route = NavDestinations.main.name) {

            val uri = Uri.parse("https://".plus(preferences.getServerAddress()))
            val user = preferences.getUser()
            val pw = preferences.getPassword()

            val owncloudClient = getOwncloudClient(LocalContext.current, uri, user, pw)
            val nextcloudClient = getNextcloudClient(LocalContext.current, uri, user, pw)

            /*Thread {
              val result2: RemoteOperationResult<UserInfo> = GetUserInfoRemoteOperation().run(nextcloudClient)
              Log.d(TAG, "GetUserInfoRemoteOperation success?" + result2.isSuccess)
            }.start()*/

            Thread {
              val readFolderRemoteOperation = ReadFolderRemoteOperation("/Music/")
              val result = readFolderRemoteOperation.execute(owncloudClient)
              Log.d(TAG, "ReadFolderRemoteOperation success?" + result.isSuccess)
            }.start()
            MainScreen()
          }
        }
      }
    }
  }

  private fun getOwncloudClient(context: Context, uri: Uri, user: String, password: String): OwnCloudClient {
    val owncloudClient = OwnCloudClientFactory.createOwnCloudClient(uri, context, true)
    owncloudClient.userId = user
    owncloudClient.credentials = OwnCloudCredentialsFactory.newBasicCredentials(user, password)
    return owncloudClient
  }

  private fun getNextcloudClient(context: Context, uri: Uri, user: String, password: String): NextcloudClient {
    return OwnCloudClientFactory.createNextcloudClient(
      uri,
      user,
      OkHttpCredentialsUtil.basic(user, password),
      context,
      true
    )
  }
}
