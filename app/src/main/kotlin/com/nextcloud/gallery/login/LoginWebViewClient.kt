package com.nextcloud.gallery.login

import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.navigation.NavHostController
import com.nextcloud.gallery.data.PreferencesFacade
import com.nextcloud.gallery.ui.model.NavDestinations

private const val TAG = "NcgWebViewClient"

class LoginWebViewClient(private val preferences: PreferencesFacade, private val navController: NavHostController) :
  WebViewClient() {
  override fun doUpdateVisitedHistory(view: WebView?, url: String?, isReload: Boolean) {
    Log.d(TAG, "url=$url")
    if (url != null && url.startsWith("nc://login/server:https://") && url.contains("&user:") && url.contains("&password:")) {
      val urlParser = UrlParser(url)
      preferences.saveLoginData(urlParser.getServerAddress(), urlParser.getUsername(), urlParser.getPassword())
      Log.d(TAG, "Account credentials saved")
      navController.navigate(NavDestinations.main.name)
      navController.clearBackStack(NavDestinations.loginWebView.name)
      navController.clearBackStack(NavDestinations.loginUrl.name)
    }
    super.doUpdateVisitedHistory(view, url, isReload)
  }
}
