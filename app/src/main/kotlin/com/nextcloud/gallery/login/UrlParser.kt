package com.nextcloud.gallery.login

class UrlParser(private val url: String) {

  fun getServerAddress(): String {
    val loginPrefix = "nc://login/server:"
    val protocolSuffix = "://"
    val protocolSuffixIndex = url.removePrefix(loginPrefix).indexOf(protocolSuffix) + loginPrefix.length
    val end = url.indexOf("&")
    val serverAddress = url.substring(protocolSuffixIndex + protocolSuffix.length, end)
    return serverAddress.removeSuffix("/")
  }

  fun getUsername(): String {
    val user = "&user:"
    val userIndex = url.indexOf(user)

    val urlSegment = url.substring(userIndex + user.length)
    val end = urlSegment.indexOf("&")
    return if (end >= 0) {
      urlSegment.substring(0, end)
    } else {
      urlSegment
    }
  }

  fun getPassword(): String {
    val password = "&password:"
    val passwordIndex = url.indexOf(password)

    val urlSegment = url.substring(passwordIndex + password.length)
    val end = urlSegment.indexOf("&")
    return if (end >= 0) {
      urlSegment.substring(0, end)
    } else {
      urlSegment
    }
  }
}
