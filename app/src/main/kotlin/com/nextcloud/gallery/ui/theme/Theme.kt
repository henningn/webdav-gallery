package com.nextcloud.gallery.ui.theme

import android.app.Activity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

@Composable
fun NextcloudGalleryTheme(
  darkTheme: Boolean = isSystemInDarkTheme(),
  loginScreen: Boolean = false,
  content: @Composable () -> Unit
) {
  val colorScheme = if (loginScreen) {
    lightColorScheme(
      primary = Color.White,
      surface = Color(0, 130, 201, 255),
      onSurface = Color.White,
      onSurfaceVariant = Color.White
    )
  } else if (darkTheme) {
    dynamicDarkColorScheme(LocalContext.current)
  } else {
    dynamicLightColorScheme(LocalContext.current)
  }

  val view = LocalView.current
  if (!view.isInEditMode) {
    SideEffect {
      val window = (view.context as Activity).window
      window.statusBarColor = colorScheme.surface.toArgb()
      WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = !darkTheme && !loginScreen
    }
  }

  MaterialTheme(
    colorScheme = colorScheme,
    content = content
  )
}
