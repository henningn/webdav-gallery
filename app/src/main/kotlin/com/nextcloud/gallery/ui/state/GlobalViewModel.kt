package com.nextcloud.gallery.ui.state

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

private const val TAG = "GlobalViewModel"

class GlobalViewModel() : ViewModel() {
  var serverAddress: String by mutableStateOf("")

  fun getLoginFlowUrl(): String {
    var url = serverAddress

    if (!url.startsWith("https://")) {
      url = "https://".plus(url)
    }
    if (!url.endsWith("/")) {
      url = url.plus("/")
    }

    return url.plus("login/flow")
  }
}
