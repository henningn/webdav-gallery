package com.nextcloud.gallery.ui.screen

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.nextcloud.gallery.R
import com.nextcloud.gallery.ui.state.GlobalViewModel
import com.nextcloud.gallery.ui.model.NavDestinations
import com.nextcloud.gallery.ui.theme.NextcloudGalleryTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginScreen(
  globalViewModel: GlobalViewModel,
  navController: NavController
) {
  val textFieldFocusRequester = remember { FocusRequester() }
  LaunchedEffect(Unit) {
    textFieldFocusRequester.requestFocus()
  }

  Surface {
    Column(
      modifier = Modifier
        .fillMaxSize()
        .wrapContentSize(Alignment.Center)
    ) {
      OutlinedTextField(
        value = globalViewModel.serverAddress,
        onValueChange = { value -> globalViewModel.serverAddress = value },
        modifier = Modifier
          .align(Alignment.CenterHorizontally)
          .width(500.dp)
          .padding(40.dp)
          .focusRequester(textFieldFocusRequester),
        label = {
          Text(stringResource(R.string.server_address).plus(" https://…"))
        },
        trailingIcon = {
          IconButton(onClick = {
            //todo: check if serverAddress is valid
            navController.navigate(NavDestinations.loginWebView.name)
          }) {
            Icon(
              Icons.Filled.ArrowForward,
              stringResource(R.string.forward)
            )
          }
        },
        supportingText = {
          Text(
            stringResource(R.string.login_supporting_text),
            modifier = Modifier.fillMaxWidth()
          )
        }
      )
    }
  }
}

@Preview(showBackground = true)
@Composable
fun LoginScreenPreview() {
  val preferences = LocalContext.current.getSharedPreferences("preview", Context.MODE_PRIVATE)
  val globalViewModel = GlobalViewModel()
  val navController = rememberNavController()

  NextcloudGalleryTheme(loginScreen = true) {
    LoginScreen(globalViewModel, navController)
  }
}
