package com.nextcloud.gallery.ui.screen

import android.os.Build
import android.webkit.WebView
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.nextcloud.gallery.data.PreferencesFacade
import com.nextcloud.gallery.login.LoginWebViewClient
import com.nextcloud.gallery.ui.state.GlobalViewModel

@Composable
fun LoginWebViewScreen(
  preferences: PreferencesFacade,
  navController: NavHostController,
  globalViewModel: GlobalViewModel
) {
  AndroidView(factory = {
    WebView(it).apply {
      webViewClient = LoginWebViewClient(preferences, navController)
      settings.allowFileAccess = false
      settings.javaScriptEnabled = true
      settings.domStorageEnabled = true
      settings.userAgentString = Build.MANUFACTURER.plus(Build.MODEL).plus(" Nextcloud Gallery")

      val headers: Map<String, String> = mapOf(Pair("OCS-APIREQUEST", "true"))
      loadUrl(globalViewModel.getLoginFlowUrl(), headers)
    }
  })
}
