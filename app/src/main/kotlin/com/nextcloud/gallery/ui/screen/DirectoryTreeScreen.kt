package com.nextcloud.gallery.ui.screen

import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.nextcloud.gallery.R
import com.nextcloud.gallery.data.DummyData
import com.nextcloud.gallery.ui.model.Directory

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DirectoryTreeScreen(directories: List<Directory>) {
  val colorSecondary = MaterialTheme.colorScheme.secondary

  Scaffold(
    topBar = {
      TopAppBar(
        title = {
          Text(stringResource(R.string.select_folders))
        },
        navigationIcon = {
          IconButton(onClick = {}) {
            Icon(Icons.Filled.ArrowBack, "backIcon")
          }
        },
        actions = {}
      )
    },
    content = { paddings ->
      Column(Modifier.padding(paddings)) {
        Row(
          modifier = Modifier
            .fillMaxWidth()
            .horizontalScroll(rememberScrollState())
            .drawBehind {
              drawLine(
                color = colorSecondary,
                start = Offset(0F, size.height),
                end = Offset(size.width, size.height),
                strokeWidth = 5F
              )
            },
          verticalAlignment = Alignment.CenterVertically
        ) {
          if (directories.size > 0) {
            val breadcrumb = directories[0].path.split("/")
            for (i in 0..breadcrumb.size - 1) {
              val foldername = if (breadcrumb[i].length > 0) breadcrumb[i] else "/"

              if (i < breadcrumb.size - 1) {
                TextButton(onClick = { }) { Text(foldername) }
                Icon(
                  painterResource(R.drawable.round_arrow_forward_ios_24),
                  "arrow forward",
                  modifier = Modifier.height(16.dp)
                )
              } else {
                Text(
                  foldername,
                  modifier = Modifier.padding(8.dp),
                  color = MaterialTheme.colorScheme.secondary,
                  style = MaterialTheme.typography.labelLarge
                )
              }
            }
          }
        }

        Column(
          modifier = Modifier
            .weight(1f)
            .verticalScroll(rememberScrollState())
        ) {
          directories.forEach { directory ->
            Row(
              modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min),
              verticalAlignment = Alignment.CenterVertically
            ) {
              Checkbox(
                checked = directory.selected,
                onCheckedChange = { value -> directory.selected = value }
              )

              val path = directory.path.substring(directory.path.lastIndexOf("/") + 1)
              if (directory.hasChildren) {
                TextButton(
                  onClick = { },
                  contentPadding = PaddingValues()
                ) {
                  Text(
                    text = path,
                    modifier = Modifier
                      .padding(start = 0.dp)
                      .fillMaxWidth()
                  )
                }
              } else {
                Text(path)
              }
            }
          }
        }

        Row(
          modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp, 8.dp),
          verticalAlignment = Alignment.CenterVertically
        ) {
          Button(
            onClick = {}
          ) {
            Text(stringResource(R.string.skip))
          }
          Spacer(modifier = Modifier.weight(1f))
          Button(
            onClick = {}
          ) {
            Text(stringResource(R.string.submit))
          }
        }
      }
    }
  )
}

@Preview(showBackground = true)
@Composable
fun DirectoryTreeScreenPreview() {
  DirectoryTreeScreen(DummyData().directories)
}
