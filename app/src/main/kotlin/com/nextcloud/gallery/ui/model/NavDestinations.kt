package com.nextcloud.gallery.ui.model

enum class NavDestinations {
  loginUrl, loginWebView, accountIncludes, main, settings
}
