package com.nextcloud.gallery.ui.component

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.nextcloud.gallery.R
import com.nextcloud.gallery.data.DummyData

@Composable
fun PhotosGrid(imagesList: ArrayList<String>) {
  LazyVerticalGrid(
    columns = GridCells.Fixed(5)
  ) {
    items(imagesList) { image ->
      Icon(
        painterResource(R.drawable.outline_image_24),
        "placeholder for photo",
        modifier = Modifier
          .wrapContentSize()
          .padding(2.dp)
          .border(1.dp, MaterialTheme.colorScheme.primary)
      )
    }
  }
}

@Preview(showBackground = true)
@Composable
fun PhotosGridPreview() {
  PhotosGrid(DummyData().imagesList)
}
