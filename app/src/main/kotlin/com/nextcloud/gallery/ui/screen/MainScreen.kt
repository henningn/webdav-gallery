package com.nextcloud.gallery.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.nextcloud.gallery.R
import com.nextcloud.gallery.ui.model.NavItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen() {
  var selectedNavItem by remember { mutableStateOf(0) }
  val navItems = listOf(
    NavItem(stringResource(R.string.photos),
      painterResource(R.drawable.baseline_image_24),
      painterResource(R.drawable.outline_image_24),
      { PhotosScreen() }
    ),
    NavItem(stringResource(R.string.folder),
      painterResource(R.drawable.baseline_folder_24),
      painterResource(R.drawable.outline_folder_24),
      { FolderScreen() }
    ),
    NavItem(
      stringResource(R.string.map),
      painterResource(R.drawable.baseline_map_24),
      painterResource(R.drawable.outline_map_24),
      { MapScreen() }
    )
  )

  Scaffold(
    topBar = {
      TopAppBar(
        title = {
          Text(text = stringResource(id = R.string.app_name))
        },
        navigationIcon = {
          IconButton(onClick = {}) {
            Icon(Icons.Filled.ArrowBack, "backIcon")
          }
        },
        actions = {
          Row() {
            IconButton(onClick = {}) {
              Icon(Icons.Outlined.Settings, "Settings")
            }
          }
        }
      )
    },
    bottomBar = {
      NavigationBar {
        navItems.forEachIndexed { index, navItem ->
          NavigationBarItem(
            selected = selectedNavItem == index,
            onClick = { selectedNavItem = index },
            icon = {
              Icon(
                if (selectedNavItem == index) {
                  navItem.iconSelected
                } else {
                  navItem.iconDeselected
                }, navItem.label
              )
            },
            label = { Text(navItem.label) }
          )
        }
      }
    },
    content = { paddings ->
      Column(Modifier.padding(paddings)) {
        navItems[selectedNavItem].screen()
      }
    }
  )
}

