package com.nextcloud.gallery.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.nextcloud.gallery.data.DummyData
import com.nextcloud.gallery.ui.component.PhotosGrid

@Preview(showBackground = true)
@Composable
fun PicturesScreenPreview() {
  PhotosScreen()
}

@Composable
fun PhotosScreen() {
  PhotosGrid(imagesList = DummyData().imagesList)
}
