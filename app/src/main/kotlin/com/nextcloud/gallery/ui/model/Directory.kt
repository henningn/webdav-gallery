package com.nextcloud.gallery.ui.model

data class Directory(
  val path: String,
  val hasChildren: Boolean,
  var selected: Boolean = false
)
