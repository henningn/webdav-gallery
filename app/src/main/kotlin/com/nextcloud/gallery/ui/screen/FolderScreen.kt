package com.nextcloud.gallery.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun FolderScreen() {
  Column(
    modifier = Modifier
      .fillMaxSize()
      .wrapContentSize(Alignment.Center)
  ) {
    Text(
      text = "Grid of every folder with a picture",
      modifier = Modifier.align(Alignment.CenterHorizontally),
      color = MaterialTheme.colorScheme.primary,
      fontWeight = FontWeight.Bold,
      textAlign = TextAlign.Center,
      style = MaterialTheme.typography.headlineLarge
    )
  }
}

@Preview(showBackground = true)
@Composable
fun FolderScreenPreview() {
  FolderScreen()
}
