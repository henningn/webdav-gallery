package com.nextcloud.gallery.ui.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.painter.Painter

data class NavItem(
  val label: String,
  val iconSelected: Painter,
  val iconDeselected: Painter,
  val screen: (@Composable () -> Unit)
)
